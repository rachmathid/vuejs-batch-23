//soal 1
const luas = (panjang, lebar) => {
	panjang * lebar
}
const keliling = (panjang, lebar) => {
	2 * (panjang + lebar)
}

//soal 2
const newFunction = (firstname,lastname) => {
	return {
		firstname,
		lastname,
		fullname: () => {
			console.log(`${firstname} ${lastname}`)
		}
	}
}

newFunction("William","Imoh").fullname()

//soal 3
const newObject = {
	firstname : "Muhammad",
	lastname : "Iqbal Mubarok",
	address : "Jalan Ranamayar",
	hobby : "playing footbal",
}

const {firstname, lastname, address, hobby} = newObject
console.log(firstname, lastname, address, hobby)

//soal 4
const west = ["will","Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)

//soal 5
const planet = "earth"
const view = "glass"

var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
const after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`

console.log(before)
console.log(after)
