//soal 1

function next_date(tanggal, bulan, tahun) {
	
	var now = new Date(tahun.toString()+'-'+bulan.toString()+'-'+tanggal.toString());
    now.setDate(now.getDate() + (tanggal + (1 - now.getDay())) % 1);
    return now;
}

var tanggal = 29
var bulan = 2
var tahun = 2020

var tomorrow = next_date(tanggal,bulan,tahun)
console.log(tomorrow)

//soal 2

function jumlah_kata(kalimat) {
	var i=0
	var jumlahkata = 1

	while (i<=kalimat.length) {
		if(kalimat.substring(i,i+1) == " ") {
			jumlahkata ++
			i++
		}

		i++
	}

	return jumlahkata
}

console.log(jumlah_kata("Hallo nama saya Muhammad Iqbal Mubarak"))
console.log(jumlah_kata("Saya Iqbal"))
