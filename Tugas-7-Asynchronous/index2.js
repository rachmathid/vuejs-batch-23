var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

readBooksPromise(10000, books[0])
	.then (sisawaktu => {
		return readBooksPromise(sisawaktu, books[1])
	})
	.then (sisawaktu => {
		return readBooksPromise(sisawaktu, books[2])	
	})
	.then (sisawaktu => {
		console.log(`Waktu Saya Yang tersisa adalah ${sisawaktu}`)
	})