//soal 1

var daftarBuah = ["2. Komodo","5. Buaya","3. Cicak","4. Ular","1. Tokek"];
daftarBuah.sort()

for(var i=0; i < daftarBuah.length; i++) {
	console.log(daftarBuah[i]);
}

//soal 2

function introduce(data) {
	var tampilkan = "Nama saya "+data.name+",Umur saya "+data.age+" tahun, alamat saya di "+data.address+", dan saya punya hobby yaitu "+data.hobby
	return tampilkan
}

var data = {name :"Rachmat", age :"30", address :"Jakarta Timur", hobby :"Nonton"}
var perkenalan = introduce(data)
console.log(perkenalan)

//soal 3

function hitung_huruf_vokal(kalimat) {
	var jmlHuruf = 0;
	var vokal = ['a','i','u','e','o','A','I','U','E','O']
	for (var i = 0 ; i < kalimat.length; i++) {
		for (var j = 0; j < vokal.length; j++) {
			if (kalimat[i] === vokal[j]) {
				jmlHuruf ++
			}
		}
	}
	return jmlHuruf
}

var hitung_1 = hitung_huruf_vokal("Rachmat Hidayat")
var hitung_2 = hitung_huruf_vokal("Nanda Amelia Sastra")

console.log(hitung_1, hitung_2)

//soal 4

function hitung(angka) {
	return angka * 2 - 2
}

console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))
